<?php
	require_once './code.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S1-Activity</title>
</head>
<body>
	<h1>In code.php, create a function named getFullAddress() that will take four arguments:</h1>
	<p>Full address: <?php echo getFullAddress('3427 Gen. Lim Street', 'Bangkal', 'Makati City', 'Metro Manila'); ?></p>

	<h2>Create another function named getLetterGrade() that uses conditional statements to output a letter representation of a given numerical grade:</h2>

	<p><?php echo getLetterGrade(76) ?></p>
</body>
</html>